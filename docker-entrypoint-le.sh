#!/bin/bash
if [[ -n "$LE_LIVE_DIR" ]] ; then
	if [[ ! -f "${LE_LIVE_DIR}/certkey.pem" || "${LE_LIVE_DIR}/fullchain.pem" -nt "${LE_LIVE_DIR}/certkey.pem" ]]; then
		cat "${LE_LIVE_DIR}/fullchain.pem" "${LE_LIVE_DIR}/privkey.pem" > "${LE_LIVE_DIR}/certkey.pem"
	fi
fi
exec /docker-entrypoint.sh "$@"
